# Bookings and Reservations
This repository is used as a project deliverable for the Udemy Course: [Building Modern Web Applications with Go](https://www.udemy.com/course/building-modern-web-applications-with-go/).  

- Built using Go version 1.17
- Uses the [chi](https://github.com/go-chi/chi) router
- Uses Alex Edwards [SCS Session Management](https://github.com/alexedwards/scs)
- [nosurf](https://github.com/alexedwards/scs)
